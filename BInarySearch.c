//
// Created by Parrot on 2019-07-04.
//

#include <stdio.h>

#define ARRAY_SIZE 7
int main() {

    // 1. Create a SORTED array
    int array[] = {40,50,60,70,80,90,100};

    // 2. What are you searching for?
    int searchValue = 100;

    // 3. Calculate the first, middle, and last positions of the array

    // 4. Make a variable to keep track of if the number was found


    // 5. Do the binary search algorithm
    // 5a. If searchValue == mid, then you found the number!
    // 5b. Otherwise:
    //  - update positions to search left side
    //  - update positions to search right side


    // 6. Print the results


} // end main